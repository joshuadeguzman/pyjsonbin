from django.apps import AppConfig


class JsonifyConfig(AppConfig):
    name = 'jsonify'
